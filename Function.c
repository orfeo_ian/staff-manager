#include "Function.h"
#include "Common.h"

Staff arr[Staff_Max];
int number = 0;

/*
*메뉴를 출력하고 입력받음
*select(메뉴 변수) 반환
*/
char Menu(void)
{
	char select[5];

	puts("\n-----------------------------------------------");
	puts("직원관리 프로그램");
	puts("1. 직원정보 입력");
	puts("2. 직원정보 출력");
	puts("3. 직원정보 검색");
	puts("4. 프로그램 종료");
	printf("선택 : ");

	gets(select);

	return select[0];
}

/*Add()
*직원정보 입력
*이름, 나이, 직급
*/
void Add()
{
	puts("\n직원정보 입력");
	printf("이름 입력:"); gets(arr[number].name);
	printf("영어이름 입력:"); gets(arr[number].english_name);
	printf("나이 입력:"); gets(arr[number].age);
	printf("직급 입력:"); gets(arr[number].position);

	number += 1;
}

/*
*직원의 정보를 검색해서 출력
*/
void Search(void)
{
	char name[20];
	int j;
	
	puts("\n직원 검색");
	puts("검색할 이름을 입력하세요!");
	gets(name);

	for (j = 0; j < Staff_Max; j++)
	{
		if (!strcmp(name,arr[j].name))
		{
			PrintInfo(j);
			break;
		}

		if (j == 9)
			puts("정보가 없습니다!");
	}
}

/*
*모든 직원의 정보 출력
*/
void PrintAllInfo(void)
{
	int k = 0;

	puts("\n직원정보 출력");
	for (k = 0; k < number; k++)
		PrintInfo(k);
}

/*
*직원정보 출력
*/
void PrintInfo(int index)
{
	printf("이름:%s\n", arr[index].name);
	printf("영어이름:%s\n", arr[index].english_name);// 영어 이름 출력
	printf("나이:%s\n",arr[index].age); // 나이 출력
	printf("직급:%s\n",arr[index].position); // 직급 출력
	printf("\n");
}