#ifndef  FUNCTION_H
#define  FUNCTION_H

#define Staff_Max 20

//직원정보 구조체
typedef struct staff {
	char name[20];//이름
	char english_name[30];//영어이름
	char age[5];//나이
	char position[20];//직급
}Staff;

char Menu(void);//메뉴출력
void Add(void);//직원정보 입력
void Search(void);//직원정보 검색
void PrintInfo(int index);//직원정보 출력
void PrintAllInfo(void);//모든 직원의 정보 출력

#endif